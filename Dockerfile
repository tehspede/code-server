FROM codercom/code-server

RUN curl -sL https://deb.nodesource.com/setup_current.x | sudo -E bash - \
  && sudo apt-get install -y nodejs build-essential
